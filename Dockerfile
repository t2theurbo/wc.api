FROM mcr.microsoft.com/dotnet/core/aspnet:2.1-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.1-stretch AS build
WORKDIR /src
COPY ["./WC.API/WC.API.csproj", "./WC.API/"]
COPY ["./WC.Core/WC.Core.csproj", "./WC.Core/"]
RUN dotnet restore "WC.API/WC.API.csproj"
COPY . .
WORKDIR "/src/WC.API"
RUN dotnet build "WC.API.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "WC.API.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "WC.API.dll"]
