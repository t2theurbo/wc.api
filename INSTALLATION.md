How to build and run docker image:

1. Navigate to root folder
2. run: 
	docker build -t wcapi .
3. After the build is complete, run:
	docker run -it -p 5000:80 wcapi
4. Navigate browser to localhost:5000 for brief intro and API details
5. Run API tests using Postman's 'Basic Authentication' with the user's phone number as the 'username' (password is irrelevant)