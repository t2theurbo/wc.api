﻿using Microsoft.AspNetCore.Mvc;

namespace WC.API.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
    }
}
