﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WC.API.Models;
using WC.API.Services;

namespace WC.API.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class USSDController : ControllerBase
    {
        private ICustomerService customerService;

        public USSDController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]AfricasTalkingModel ATModel)
        {
            var customer = await customerService.Authenticate(ATModel.PhoneNumber);

            if (customer == null)
                return BadRequest(new { message = "Invalid credentials." });

            return Ok(customer);
        }

        [HttpGet("myInfo")]
        public IActionResult GetMyInfo()
        {
            return Ok(customerService.LoggedInCustomer);
        }
    }
}
