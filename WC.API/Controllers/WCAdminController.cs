﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WC.API.Models;
using WC.API.Services;

namespace WC.API.Controllers
{
    [Authorize(Policy = "Admin")]
    [Route("[controller]")]
    [ApiController]
    public class WCAdminController : ControllerBase
    {
        private ICustomerService customerService;

        public WCAdminController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        [HttpGet("customers")]
        public async Task<IActionResult> GetCustomers()
        {
            var customers = await customerService.GetCustomers();
            return Ok(customers);
        }

        [HttpGet("customers/{id}")]
        public async Task<IActionResult> GetCustomerById(int id)
        {
            var customer = await customerService.GetCustomerById(id);

            //todo: better error handling
            if (customer == null)
                return BadRequest(new { message = "Unable to find customer with id of " + id });

            return Ok(customer);
        }

        [HttpPost("customers/add")]
        public async Task<IActionResult> AddCustomer([FromBody]CustomerModel customer)
        {
            var newCustomer = await customerService.AddCustomer(customer);

            //todo: better error handling
            if (newCustomer == null)
                return BadRequest(new { message = "Unable to save customer. A unique phone number is required." });

            return Ok(newCustomer);
        }
    }
}
