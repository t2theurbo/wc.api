﻿using System;

namespace WC.API.Models
{
    public class AfricasTalkingModel
    {
        public string SessionId { get; set; }
        public string PhoneNumber { get; set; }
        public string NetworkCode { get; set; }
        public string Text { get; set; }
    }
}
