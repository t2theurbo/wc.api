﻿using System;

namespace WC.API.Models
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Village { get; set; }
        public DateTime RegistrationDate { get; set; }

        //temp hack for proof of concept
        public bool IsAdmin
        {
            get
            {
                return Name.Contains("Admin", StringComparison.OrdinalIgnoreCase);
            }
        }

        /*
         * phone number acts like password in dummy example
         * needs to be unique keyed in DB etc
         * proper roles for access, can have multiple 'users' in one 'customer'?
         *
         * public string Password { get; set; }
         */
        public string PhoneNumber { get; set; }
    }
}
