﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WC.API.Models;
using WC.API.Utility;
using WC.Core;

namespace WC.API.Services
{
    public class CustomerService : ICustomerService
    {
        public CustomerModel LoggedInCustomer { get; set; }

        public async Task<CustomerModel> Authenticate(string phoneNumber)
        {
            var customer = await Task.Run(() => new CustomerRepository().GetCustomers()
                .SingleOrDefault(x => x.PhoneNumber == phoneNumber));

            if (customer == null)
                return null;

            var customerModel = customer.MapCustomerToModel();
            LoggedInCustomer = customerModel;
            return customerModel;
        }

        public async Task<IEnumerable<CustomerModel>> GetCustomers()
        {
            return await Task.Run(() => new CustomerRepository().GetCustomers().Select(c => c.MapCustomerToModel()));
        }

        public async Task<CustomerModel> GetCustomerById(int id)
        {
            var customer = await Task.Run(() => new CustomerRepository().GetCustomers().FirstOrDefault(c => c.Id == id));

            if (customer == null)
                return null;

            return customer.MapCustomerToModel();
        }

        public async Task<CustomerModel> AddCustomer(CustomerModel customer)
        {
            var newCustomer = new CustomerRepository().AddCustomer(customer.MapModelToCustomer());

            if (newCustomer == null)
                return null;

            return await Task.Run(() => newCustomer.MapCustomerToModel());
        }
    }
}