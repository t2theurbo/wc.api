﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WC.API.Models;

namespace WC.API.Services
{
    public interface ICustomerService
    {
        CustomerModel LoggedInCustomer { get; set; }

        Task<CustomerModel> Authenticate(string phoneNumber);
        Task<IEnumerable<CustomerModel>> GetCustomers();
        Task<CustomerModel> GetCustomerById(int id);
        Task<CustomerModel> AddCustomer(CustomerModel customer);
    }
}