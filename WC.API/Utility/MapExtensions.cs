﻿using System;
using WC.API.Models;
using WC.Core;

namespace WC.API.Utility
{
    public static class MapExtensions
    {
        public static CustomerModel MapCustomerToModel(this Customer customer)
        {
            return new CustomerModel
            {
                Id = customer.Id,
                Name = customer.Name,
                PhoneNumber = customer.PhoneNumber,
                Village = customer.Village,
                RegistrationDate = customer.RegistrationDate
            };
        }

        public static Customer MapModelToCustomer(this CustomerModel model)
        {
            return new Customer
            {
                Id = model.Id,
                Name = model.Name,
                PhoneNumber = model.PhoneNumber,
                Village = model.Village,
                RegistrationDate = model.RegistrationDate
            };
        }
    }
}
