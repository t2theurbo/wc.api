﻿using System;
namespace WC.Core
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Village { get; set; }
        public DateTime RegistrationDate { get; set; }

        //etc
        public Jurisdiction Jurisdiction { get; set; }

        public Customer()
        {
        }
    }
}
