﻿using System;
namespace WC.Core
{
    public class Jurisdiction
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ISOCode { get; set; }

        public Jurisdiction()
        {
    
        }
    }
}
