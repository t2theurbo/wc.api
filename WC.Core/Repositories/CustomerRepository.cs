﻿using System;
using System.Collections.Generic;
using System.Linq;
using WC.Core.Utility;

namespace WC.Core
{
    public class CustomerRepository
    {
        //todo: db calls
        public List<Customer> GetCustomers()
        {
            List<Customer> customers = (List<Customer>)BasicCache.Get("allCustomers");
            if(customers == null)
            {
                BasicCache.AddOrUpdate("allCustomers", initCustomers);
                return initCustomers;
            }

            return customers;
        }

        public Customer AddCustomer(Customer customer)
        {
            List<Customer> customers = (List<Customer>)BasicCache.Get("allCustomers");
            if (customers == null)
                BasicCache.AddOrUpdate("allCustomers", initCustomers);
            
            //basic validation, should throw exception if matching phone numbers
            if (customers.Any(c => c.PhoneNumber.Equals(customer.PhoneNumber, System.StringComparison.OrdinalIgnoreCase)))
                return null;

            //save
            customer.Id = customers.Max(c => c.Id) + 1;
            customers.Add(customer);

            BasicCache.AddOrUpdate("allCustomers", customers);

            return customer;
        }


        /*
         * hardcoded customers for cache init
         *
         */
        private List<Customer> initCustomers = new List<Customer>
        {
            new Customer {
                Id = 1,
                Name = "WC Admin",
                Village = "Support team",
                RegistrationDate = new System.DateTime(2000, 1, 1),
                PhoneNumber = "+1"
            },
            new Customer {
                Id = 10,
                Name = "John Snow",
                Village = "Winterfell",
                RegistrationDate = new System.DateTime(2017, 5, 20),
                PhoneNumber = "+31123"
            },
            new Customer {
                Id = 20,
                Name = "Jaime Lannister",
                Village = "Casterly Rock",
                RegistrationDate = new System.DateTime(2018, 6, 13),
                PhoneNumber = "+31456"
            }
        };
    }
}
