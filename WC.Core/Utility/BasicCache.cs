﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace WC.Core.Utility
{
    public static class BasicCache
    {
        public static MemoryCache cache = new MemoryCache(new MemoryCacheOptions());

        public static bool AddOrUpdate(string key, object item)
        {
            cache.Set(key, item);
            return true;
        }

        public static object Get(string key)
        {
            return cache.Get(key);
        }
    }
}
